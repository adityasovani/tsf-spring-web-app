# tsf-spring-app
  
## Description
This project contains CRUD operations using Spring boot framework.


## Prerequisites for running this project
#### SQL
Make sure you have SQL server running.
Create SQL database named ```tsfdemo```
```sql
  CREATE DATABASE tsfdemo;
```
then create a table ```stu_data```with 3 columns Id, Name, Pointer.
```sql
 CREATE TABLE stu_data(Id int,Name Varchar(255),Pointer Double );
```
Mention database username and password in ```application.properties```file.
```properties
  spring.datasource.url = jdbc:mysql://localhost:3306/tsfdemo?usessl = false
  spring.datasource.username = root
  spring.datasource.password = pass123

# Hibernate Props
  spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5InnoDBDialect

#Hibernate ddl auto (create, create-drop, validate, update)
  spring.jpa.hibernate.ddl-auto = update
```
 #### Postman

##### 1.For Create:
 Select POST request. Enter url ```http://localhost:8080/addStudent```. Go to body tab select type: application/json. 

sample body code:
```json
  {
      "id":"2",
      "name":"Aditya",
      "pointer":"8.04"
  }
```
Send the request.
##### 2.For Read:
 Select GET request.Enter url ```http://localhost:8080/showAll```. Send the request.
##### 3.For Search (by Id):
 Select GET request.Enter url ```http://localhost:8080/search/id```. Send the request.
##### 4. For Update:
 Select PUT request. Enter url ```http://localhost:8080/updateStudent/id```. Go to body tab select type: application/json. 

sample body code:
```json
  {
      "name":"Rajat",
      "pointer":"8.56"
  }
```
Send the request.
##### 5.For Delete:
 Select DELETE request. Enter url ```http://localhost:8080/delete/id```.